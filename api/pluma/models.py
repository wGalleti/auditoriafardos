from django.db import models


class Fardao(models.Model):
    TIPO = (
        (1, 'Fardão'),
        (2, 'Fardinho'),
    )
    numero = models.CharField(max_length=10)
    tipo = models.IntegerField(default=2, choices=TIPO)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    usuario = models.ForeignKey('auth.User', on_delete=models.CASCADE)

    def __str__(self):
        return self.numero

    class Meta:
        verbose_name_plural = 'contagem dos fardões'
        verbose_name = 'contagem de fardão'


class FardaoJustificativa(models.Model):
    TIPO = (
        (1, 'Fardão'),
        (2, 'Fardinho'),
    )
    numero = models.CharField(max_length=10)
    tipo = models.IntegerField(default=2, choices=TIPO)
    justificativa = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    usuario = models.ForeignKey('auth.User', on_delete=models.CASCADE)

    def __str__(self):
        return self.numero

    class Meta:
        verbose_name_plural = 'justificativas dos fardões'
        verbose_name = 'justificativa do fardão'
