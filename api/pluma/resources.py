from import_export import resources
from import_export.fields import Field

from pluma.models import Fardao, FardaoJustificativa


class FardaoResource(resources.ModelResource):
    class Meta:
        model = Fardao


class FardaoJustificativaResource(resources.ModelResource):
    usuario = Field()
    tipo = Field()

    class Meta:
        model = FardaoJustificativa

    def dehydrate_usuario(self, obj):
        return obj.usuario.username

    def dehydrate_tipo(self, obj):
        return obj.get_tipo_display()
