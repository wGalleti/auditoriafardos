from django.contrib import admin
from import_export.admin import ImportExportMixin

from pluma.models import Fardao, FardaoJustificativa
from pluma.resources import FardaoJustificativaResource, FardaoResource


@admin.register(Fardao)
class FardaoAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ['numero', 'tipo', 'usuario']
    resource_class = FardaoResource


@admin.register(FardaoJustificativa)
class FardaoJustificativaAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = ['numero', 'tipo', 'justificativa', 'usuario']
    resource_class = FardaoJustificativaResource
