from rest_framework import routers
from pluma.views import FardaoJustificativaViewSet, FardaoViewSet

r = routers.DefaultRouter(trailing_slash=True)
r.register('fardoes', FardaoViewSet)
r.register('justificativas', FardaoJustificativaViewSet)

urlpatterns = r.urls
