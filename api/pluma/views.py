from rest_framework import viewsets
from pluma.serializers import FardaoJustificativa, Fardao, FardaoJustificativaSerializer, FardaoSerializer


class UsuarioFilterViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        qs = self.queryset
        if self.request.user is not None:
            if not self.request.user.is_superuser:
                qs = self.queryset.filter(usuario=self.request.user)
        return qs


class FardaoJustificativaViewSet(UsuarioFilterViewSet):
    queryset = FardaoJustificativa.objects.all()
    serializer_class = FardaoJustificativaSerializer


class FardaoViewSet(UsuarioFilterViewSet):
    queryset = Fardao.objects.all()
    serializer_class = FardaoSerializer
