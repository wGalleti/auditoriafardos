from rest_framework import serializers
from pluma.models import Fardao, FardaoJustificativa


class FardaoSerializer(serializers.ModelSerializer):
    tipo_display = serializers.SerializerMethodField()
    usuario_display = serializers.SerializerMethodField()

    def get_usuario_display(self, obj):
        return obj.usuario.username

    def get_tipo_display(self, obj):
        return obj.get_tipo_display()

    class Meta:
        model = Fardao
        fields = '__all__'


class FardaoJustificativaSerializer(serializers.ModelSerializer):
    tipo_display = serializers.SerializerMethodField()
    usuario_display = serializers.SerializerMethodField()

    def get_usuario_display(self, obj):
        return obj.usuario.username

    def get_tipo_display(self, obj):
        return obj.get_tipo_display()

    class Meta:
        model = FardaoJustificativa
        fields = '__all__'
