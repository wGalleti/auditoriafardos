import axios from '../plugins/axios'

const namespaced = true

const state = {
  lista: []
}

const mutations = {
  ADD_DATA (state, payload) {
    state.lista = payload
  }
}

const actions = {
  addFardao: (context, form) => {
    return axios.post('auditoria/fardoes/', {...form, usuario: context.rootState.auth.user.pk})
  },
  addJustificativa: (context, form) => {
    return axios.post('auditoria/justificativas/', {...form, usuario: context.rootState.auth.user.pk})
  },
  getData: (context, tipo) => {
    return axios.get(`auditoria/${tipo}/`).then(
      res => {
        context.commit('ADD_DATA', res.data)
      }
    ).catch(
      err => console.error(err)
    )
  },
  removeData: (context, payload) => axios.delete(`auditoria/${payload.formType}/${payload.id}/`)
}

export default {
  namespaced,
  state,
  mutations,
  actions
}
