import axios from '../plugins/axios'

const namespaced = true

const state = {
  token: null,
  user: null
}

const mutations = {
  LOGOUT (state) {
    state.user = null
    state.token = null
  },
  AUTH (state, payload) {
    state.user = payload
  },
  LOGIN (state, payload) {
    state.token = payload.key
  }
}

const actions = {
  login: (context, payload) => axios.post('rest-auth/login/', payload).then(
    data => {
      context.commit('LOGIN', data.data)
    }
  ),
  logout: (context) => axios.post('rest-auth/logout/').then(
    () => {
      context.commit('LOGOUT')
    }
  ).catch(
    () => {
      location.reload()
      context.commit('LOGOUT')
    }
  ),
  check: (context) => new Promise((resolve, reject) => {
    if (context.state.token !== null) {
      axios.defaults.headers.common['Authorization'] = 'Token ' + context.state.token
      axios.get('rest-auth/user/').then(
        data => {
          context.commit('AUTH', data.data)
          resolve()
        }
      ).catch(
        error => {
          context.commit('LOGOUT')
          reject(error.response.data)
        }
      )
    } else {
      throw new Error('Não foi localizado chave de acesso. Efetue login novamente')
    }
  })
}

const getters = {
  notLogged: state => state.user === null
}

export default {
  namespaced,
  state,
  mutations,
  actions,
  getters
}
