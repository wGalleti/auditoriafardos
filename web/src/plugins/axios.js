import axios from 'axios';

const client = axios.create({
  baseURL: 'http://localhost:8000/',
  // baseURL: 'https://jsonplaceholder.typicode.com',
  timeout: 5000,
});

export default client;
